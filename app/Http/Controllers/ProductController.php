<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
	public function index(){

		$products = $this->getProducts();

		return view('products.index')->with('products', $products);	
	}

	public function edit($id){
		$product = new Product();
		$products = $this->getProducts();
		
		if($id == -1)
			$product->init();
		else
			$product = $products->find($id);

		return view('products.edit')->with('product', $product);
	}

	public function getProducts()
	{

		$collection = new Collection();
		if(Storage::exists('products.json'))
			{
				$products = json_decode(Storage::get('products.json'));

				foreach ($products as $product_json) {
					$product = new Product();
					$product->fillData((array)$product_json);
					$collection->add($product);
				}
			}
			return $collection;

		}


		public function save(){

			$validatedData = request()->validate([
				'id' => 'required',
				'name' => 'required',
				'price' => 'required',
				'quantity' => 'required'
			]);

			$products = $this->getProducts();


			if($validatedData['id'] >= 0)
			{
				$product = $products->find($validatedData['id']);
				$product->fillData($validatedData);
			}
			else
			{
				$validatedData['id'] = $products->count();
				$product = new Product();
				$product->fillData($validatedData);
				$products->add($product);	
			}

			Storage::put('products.json', $products->toJson(), 'public');

			return redirect()->route('products');

		}
	}
