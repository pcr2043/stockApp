<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //

	protected $appends = ['total'];

	public function getTotalAttribute($value)
	{
		return number_format(doubleval($this->price) * intval($this->quantity),2);
	}

	public function init(){
		
		$this->id =-1;
		$this->name = '';
		$this->quantity ='';
		$this->price = '';
	}

	public function fillData($data){
		$this->id = $data['id'];
		$this->name = $data['name'];
		$this->quantity = $data['quantity'];
		$this->price = $data['price'];
	}
}
