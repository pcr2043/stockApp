@extends('layout')


@section('content')
<div class="row">
	<div class="col-xs-12">
		<h1>Products</h1>
		<a href="/product/-1" type="button" class="btn btn-primary">CREATE PRODUCT</a>


		<table class="table table-hover">
			<thead>
				<tr>
					<th>ID</th>
					<th>NAME</th>
					<th>QUANTITY</th>
					<th>PRICE</th>
					<th>TOTAL</th>
					<th>ACTION</th>
				</tr>
			</thead>
			<tbody>
				@foreach($products as $product)
					<tr>
						<td>{{ $product->id }}</td>
						<td>{{ $product->name}}</td>
						<td>{{ $product->quantity }}</td>
						<td>{{ $product->price }}</td>
						<td>{{ $product->total }}</td>.
						<td><a href="/product/{{ $product->id }}" type="button" class="btn btn-success">EDIT</button></td>
					</tr>
				@endforeach
			</tbody>

		</table>
	</div>
</div>
@endsection