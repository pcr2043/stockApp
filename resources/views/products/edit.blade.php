@extends('layout')

@section('content')
<div class="row">
	<div class="col-xs-12">

		@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach


			</ul>
		</div>
		@endif

		<form method="post" action="/product">
			<legend>Product</legend>
			{{ csrf_field() }}
			<input type="hidden" name="id" value="{{ $product->id }}">
			<div class="form-group">
				<label>Name</label>	
				<input type="text" name="name" value="{{ $product->name }}" class="form-control">
			</div>

			<div class="form-group">
				<label>Quantity</label>
				<input type="quantity" name="quantity" value="{{ $product->quantity }}" class="form-control">
			</div>

			<div class="form-group">
				<label>Price</label>
				<input type="text" name="price" value="{{ $product->price }}" class="form-control">
			</div>
			<a href="/" class="btn btn-danger">CANCEL</a>
			<button type="submit" class="btn btn-primary">SAVE</button>

		</form>
	</div>
</div>
@endsection